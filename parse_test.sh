#!/bin/bash

python parse_bgp_updates.py --input dumps/rrc01/2016.03.09/ dumps/rrc01/2016.03.10/ --start_date 16.3.9.15 --end_date 16.3.10.15 --as_path 16509 --print_updates True --save_updates saved/rrc01.bgp &
python parse_bgp_updates.py --input dumps/rrc07/2016.03.09/ dumps/rrc07/2016.03.10/ --start_date 16.3.9.15 --end_date 16.3.10.15 --as_path 16509 --print_updates True --save_updates saved/rrc07.bgp &
python parse_bgp_updates.py --input dumps/rrc11/2016.03.09/ dumps/rrc11/2016.03.10/ --start_date 16.3.9.15 --end_date 16.3.10.15 --as_path 16509 --print_updates True --save_updates saved/rrc11.bgp &
python parse_bgp_updates.py --input dumps/rrc14/2016.03.09/ dumps/rrc14/2016.03.10/ --start_date 16.3.9.15 --end_date 16.3.10.15 --as_path 16509 --print_updates True --save_updates saved/rrc14.bgp &
python parse_bgp_updates.py --input dumps/rrc16/2016.03.09/ dumps/rrc16/2016.03.10/ --start_date 16.3.9.15 --end_date 16.3.10.15 --as_path 16509 --print_updates True --save_updates saved/rrc16.bgp