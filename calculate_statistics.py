#!/usr/bin/python

#CODE BASE TAKEN FROM: netcourier/DataConverter/calculate_statistics.py

import argparse
import logging
import os
import sys
import numpy as np
import pandas as pd
import scipy.stats as sc
import joblib
import itertools

from netcourier.DataConverter.common.zones import ZONES_SHORT, match_zones_from_dir_udp

logging.basicConfig(format="(%(process)d)\t%(asctime)s\t%(levelname)s\t%(message)s", datefmt='%H:%M:%S', level=logging.DEBUG)
log = logging.getLogger("calculate statistics")

SECOND     = 1
MINUTE     = 60*SECOND
HOUR       = 60*MINUTE
DAY        = 24*HOUR
MAX_WINDOW = 1000
MIN_SLICE_SIZE = 10

def load_latencies(src, dst):
    if_name = os.path.join(FLAGS.src_dir, FLAGS.src_nameformat.format(src=src,dst=dst))
    assert os.path.isfile(if_name)

    print "Filename: {}"   .format(if_name)
    dataset = np.genfromtxt(if_name, delimiter=' ', usecols=(0, 1), dtype=(float, float))
    if FLAGS.recent != 0:
        recent_window_stats(dataset)
    else:
        apply_window_and_calculate_stats(dataset)

def packet_loss(dataset_slice):
    start_time = dataset_slice[0][0]
    end_time = dataset_slice[len(dataset_slice)-1][0]

    raw_dataset = np.genfromtxt(FLAGS.raw_dataset, delimiter=' ', skip_header=2, skip_footer=1, usecols=(2, 3), dtype=(int, float))
    start_index = raw_dataset[np.where(raw_dataset[:,1] == start_time)]
    start_pkt = raw_dataset[start_index][0]
    end_index = raw_dataset[np.where(raw_dataset[:,1] == end_time)]
    end_pkt = raw_dataset[end_index][0]

    raw_dataset_slice_pkts = raw_dataset[start_index:end_index, 0]
    all_pkts = np.arange(start_pkt, end_pkt+1)
    lost_pkts = np.setdiff1d(all_pkts, raw_dataset_slice_pkts)
    pkt_loss_ratio = len(lost_pkts) / len(all_pkts)
    
    print "Packets lost: ", len(lost_pkts)
    print "Packet loss ratio: ", pkt_loss_ratio

    return pkt_loss_ratio

def recent_window_stats(dataset):
    i = 1
    start_time = dataset[len(dataset)-1][0]
    reversed_dataset = reversed(dataset[0:len(dataset)-1, :])
    for [time, _] in reversed_dataset:
        i += 1
        if (start_time - time) >= FLAGS.recent:
            break

    recent_slice = dataset[len(dataset)-1-i:, :]

    stats_dic = {}
    for s in FLAGS.stats_list:
        stats_dic[s] = 0

    for s in FLAGS.stats_list:
        if s == "percentile-1":
            stat_val = np.percentile(recent_slice[:, 1], 1)
            #print "1st  Percentile: {}".format(stat_val)
        elif s == "median":
            stat_val = np.median(recent_slice[:, 1])
            #print "         Median: {}".format(stat_val)
        elif s == "percentile-99":
            stat_val = np.percentile(recent_slice[:, 1], 99)
            #print "99th Percentile: {}".format(stat_val)
        elif s == "kurtosis":
            stat_val = sc.kurtosis(recent_slice[:, 1], bias=True, fisher=True)
            #print "       Kurtosis: {}".format(stat_val)
        elif s == "packet-loss":
            stat_val = packet_loss(recent_slice[:, 1])
            #print "       Packet loss: {}".format(stat_val)
        elif s == "variance":
            stat_val = np.var(recent_slice[:, 1])
            #print "       Variance: {}".format(stat_val)
        elif s == "skewness":
            stat_val = sc.skew(recent_slice[:, 1], bias=True)
            #print "       Skewness: {}".format(stat_val)

        stats_dic[s] = stat_val

def apply_window_and_calculate_stats(dataset):
    """
    Parse the samples of a dataset and slice the samples based on the given window value (in seconds).
    For each slice, calculate the requested statistics and move to the next slice until the dataset is exhausted.
    """
    if dataset.size == 0:
        log.warning("Dataset array is empty")
        return

    # The dimensionality of the dataset
    samples, columns = np.shape(dataset)
    #print np.shape(dataset)

    # The resulting statistics must have the same dimensionality as the dataset.
    # We create a dictionary of arrays. Each key corresponds to a different statistic (e.g., median)
    stats_dic = {}
    for s in FLAGS.stats_list:
        stats_dic[s] = np.empty(shape=(samples,1))
        assert np.shape(stats_dic[s])[0] == samples
    #print stats_dic

    for (x,y), value in np.ndenumerate(dataset):
        #print (dataset[x][0], dataset[x][1])

        # We cannot go further in the dataset, end of samples
        if x+1 >= samples:
            continue

        # Initial timestamp of this window
        start_time  = dataset[x][0]
        start_index = x
        #print x, start_time

        # Parse the successive samples looking for a sample that is `window` seconds far from the initial one (i.e., start_time)
        for (new_x,new_y), new_value in np.ndenumerate(dataset[start_index+1:, :]):
            next_time = dataset[start_index+new_x][0]
            #print new_x, next_time

            # We got it
            if (next_time - start_time) >= FLAGS.window:
                next_index = start_index+1+new_x
                #print (next_time - start_time)
                #print "{} samples in window".format(new_x)
                break
                #sys.exit(0)

        dataset_slice = dataset[start_index:next_index, :]
        #print dataset_slice[:, 1]

        # In case a slice contains inadequate (with respect to MIN_SLICE_SIZE value) number of samples, do not take it into account
        slice_samples = np.shape(dataset_slice)[0]
        if slice_samples < MIN_SLICE_SIZE:
            log.warning("The size of this data slice is {} < {} samples".format(slice_samples, MIN_SLICE_SIZE))
            continue

        # Compute the statistics of this slice
        for s in FLAGS.stats_list:
            if s == "percentile-1":
                stat_val = np.percentile(dataset_slice[:, 1], 1)
                #print "1st  Percentile: {}".format(stat_val)
            elif s == "median":
                stat_val = np.median(dataset_slice[:, 1])
                #print "         Median: {}".format(stat_val)
            elif s == "percentile-99":
                stat_val = np.percentile(dataset_slice[:, 1], 99)
                #print "99th Percentile: {}".format(stat_val)
            elif s == "kurtosis":
                stat_val = sc.kurtosis(dataset_slice[:, 1], bias=True, fisher=True)
                #print "       Kurtosis: {}".format(stat_val)
            elif s == "packet-loss":
                stat_val = packet_loss(dataset_slice[:, 1])
                #print "       Packet loss: {}".format(stat_val)
            elif s == "variance":
                stat_val = np.var(dataset_slice[:, 1])
                #print "       Variance: {}".format(stat_val)
            elif s == "skewness":
                stat_val = sc.skew(dataset_slice[:, 1], bias=True)
                #print "       Skewness: {}".format(stat_val)

            stats_dic[s][start_index] = stat_val

    for s in FLAGS.stats_list:
        print np.shape(stats_dic[s])
    print stats_dic

    sys.exit(0)

def cut_days(src, dst, cut_start, cut_end):
    if_name = os.path.join(FLAGS.src_dir, FLAGS.src_nameformat.format(src=src,dst=dst))
    log.info("Cutting file %s", if_name)

    data = np.genfromtxt(
        if_name,
        delimiter=' ',
        usecols=(0, 1),
        dtype=(float, float)
    )
    
    assert (cut_end <= data[-1][0]), "End cut range is outside of the data set {0} > {1}".format(
        cut_end,
        data[-1][0]
    )
    
    cut_indexes = []
    cut_indexes += list(np.where( data.T[0] < cut_start)[0])
    print "prefix: ", len(cut_indexes)
    cut_indexes += list(np.where( data.T[0] > cut_end)[0])
    print "postfix: ", len(cut_indexes)
    
    data = np.delete(data, np.asarray(cut_indexes, dtype=int), axis=0)
    
    of_name = os.path.join(FLAGS.out_dir, FLAGS.out_nameformat.format(src=src, 
                                                                      dst=dst))
    log.info("Saving file %s", of_name)    
    np.savetxt(of_name, data, delimiter=' ')

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Read already parsed data (Time-Latency) and enhance this data with statistics such as percentiles, kurtosis")
    parser.add_argument("--src_dir", default="/home/kirillb/data_sets_nc/DATASET-1-AZ-PARSED/")
    parser.add_argument("--src_nameformat", default="[{src}]-[{dst}].u.cut")
    parser.add_argument("--out_dir", default="/home/marcus/marcus_marcus-project/mytests/")
    parser.add_argument("--out_nameformat", default="[{src}]-[{dst}].u.stats")
    parser.add_argument("--n_jobs", default=1, type=int)
    parser.add_argument("--stats_list", nargs='*', default=["median"])
    parser.add_argument("--raw_dataset")
    parser.add_argument("--window", default=60, type=int, help=""" Time window in seconds over which we calculate the statistics. """)
    parser.add_argument("--recent", default=0, type=int, help=""" If non-zero, look at the most recent seconds instead (begin from the back) """)
    
    # The statistics that we currently calculate over the data
    supported_stats = ["median", "percentile-1", "percentile-99", "kurtosis", "packet-loss", "variance"]
    supported_source_formats = [".cut", ".data"]
    default_out_format = "[{src}]-[{dst}].u.stats"

    FLAGS = parser.parse_args()
    # If any of the requested statistics deos not exist in the list of supported statistics, raise an error
    if len( set(FLAGS.stats_list).difference(supported_stats) ) > 0:
        raise RuntimeError("Invalid statistics requested. Supported statistics are %s", supported_stats)
    
    print "    Source directory: {}".format(FLAGS.src_dir)
    FLAGS.stats_list = list(sorted(set(FLAGS.stats_list)))
    print "Requested Statistics: {}".format(FLAGS.stats_list)

    # Avoid various unwanted cases
    if not FLAGS.src_nameformat.endswith(tuple(supported_source_formats)):
        raise RuntimeError("Invalid source data format. Data files should end with {}".format(supported_source_formats))
    if (FLAGS.src_nameformat == FLAGS.out_nameformat) and (FLAGS.src_dir == FLAGS.out_dir):
        log.warning("Source data directory and format match with output data directory and format. The output will overwrite the input if I continue.")
        log.warning("To avoid this, I set out_nameformat={}".format(default_out_format))
        FLAGS.out_nameformat = default_out_format

    # Time window over which we calculate the requested statistics
    if (FLAGS.window <= 0) or (FLAGS.window > MAX_WINDOW):
        raise RuntimeError("The time window to calculate the statistics must be positive number less that {}".format(MAX_WINDOW))

    # Find the zones that take part in the data exchange
    ZONES, ZONES_SHORT, ZONES_TO_SHORT_NAME, _ = match_zones_from_dir_udp(FLAGS.src_dir, ".u.cut")
    print "               Zones: {}".format(ZONES_SHORT)
    
    log.info("Start")
   
    tasks = (
            joblib.delayed(load_latencies)(src, dst)
            for src, dst in itertools.permutations(ZONES, r=2)
    )
    samples = joblib.Parallel(n_jobs=1, verbose=0)(tasks)
    
    log.info("End")