#!/usr/bin/python
import argparse
import logging
import os
import sys
import numpy as np
import itertools
import time

def load_file(file):
    print "Filename: %s" %file
    ts = np.genfromtxt(file, delimiter=' ', usecols=(0, 1), dtype=(float, float))
    return ts

def trim_ts(ts, rows):
    print "Trimming %d rows" %rows
    trimmed_ts = ts[0:-rows]
    return trimmed_ts

def create_binary_ts(ts1, ts2):
    binary_ts = np.ndarray(shape=(ts1.size / len(ts1[0]), len(ts1[0])))
    reordered = 0
    for i, [ts1_time,ts1_lat] in enumerate(ts1, start=0):
        ts2_lat = ts2[i][1]
        if ts1_lat > ts2_lat:
            if reordered == 1:
                binary_ts[i] = [ts1_time,0]
            else:
                binary_ts[i] = [ts1_time,1]
            reordered = 1
        else:
            if reordered == 1:
                binary_ts[i] = [ts1_time,1]
            else:
                binary_ts[i] = [ts1_time,0]
            reordered = 0

    return binary_ts

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "Syntax: %s ts1 ts2 output_file" %sys.argv[0]
        sys.exit(1)

    ts1 = load_file(sys.argv[1])
    ts2 = load_file(sys.argv[2])

    if ts1.size == 0 or ts2.size == 0:
        print "Size of time series needs to be larger than 0"
        sys.exit(1)

    print "Size before trim: ts1 size: %d, ts2 size: %d" %(len(ts1),len(ts2))
    if ts1.size > ts2.size:
        rows = (ts1.size-ts2.size) / len(ts1[0])
        ts1 = trim_ts(ts1, rows)
    elif ts2.size > ts1.size:
        rows = (ts2.size-ts1.size) / len(ts2[0])
        ts2 = trim_ts(ts2, rows)
    print "Size after trim: ts1 size: %d, ts2 size: %d" %(len(ts1),len(ts2))

    binary_ts = create_binary_ts(ts1, ts2)
    np.savetxt(sys.argv[3], binary_ts, delimiter=' ')

    sys.exit(0)